// Здесь описан частный случай решения задачи
// Для production кода я бы выбрал moment или dayjs
// но в рамках тестового задания данная функция будет просто конвертировать дату
// из исходного формата "yyyy-MM-dd" в "dd.MM.yyyy"
/**
 * Трансформирует строку из формата "yyyy-MM-dd" в "dd.MM.yyyy"
 * @param dateString
 * @returns {string}
 */
export const dateTransformer = (dateString) => {
  return dateString.split('-').reverse().join('.');
};

/**
 * Трансформирует число (как int так и double) в строку
 * с разделяющим 3 симола пррбелом
 * @param {string} currency
 * @returns {string}
 */
export const currencyTransformer = (currency) => {
  /*
    Самый простой вариант
    const formatter = new Intl.NumberFormat('ru-RU', {
      style: 'currency',
      currency: 'RUB',
    });

    return formatter.format(currency);
  */
  const reverse = (string) => {
    if (string.length) return string.split('').reverse().join('');
    return string;
  };

  const divideByThreeSymbols = (string) => {
    if (string.length > 3) {
      return reverse(string)
        .match(/.{1,3}/g)
        .map(reverse)
        .reverse()
        .join(' ');
    }
    return string;
  };

  if (typeof currency !== 'string') {
    throw TypeError('currency must be a string!');
  }

  const currencyString = currency.replace(' ', '');

  const hasDots = currencyString.match(/\./g);
  if (hasDots && hasDots.length > 0) {
    const [beforeDot, afterDot] = currencyString.split('.');

    if (beforeDot.length) {
      return `${divideByThreeSymbols(beforeDot)}.${afterDot.slice(0, 2) || ''}`;
    }

    return divideByThreeSymbols(afterDot);
  }

  return divideByThreeSymbols(currencyString);
};

export const numberTransformer = (stringWithNumber) => {
  if (typeof stringWithNumber !== 'string') {
    return undefined;
  }
  const result = stringWithNumber
    .replace(/[^0-9.,]/g, '')
    .replace(',', '.');

  const l = result.split('.');
  if (l.length > 2) {
    return result.split('.').slice(0, 2).join('.');
  }

  return result;
};
