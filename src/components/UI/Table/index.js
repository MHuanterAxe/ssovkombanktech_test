import Vue from 'vue';
import TableContent from './TableContent.vue';
import TableHeader from './TableHeader.vue';
import TableRow from './TableRow.vue';
import TableCeil from './TableCeil.vue';

Vue.component('ui-table-content', TableContent);
Vue.component('ui-table-row', TableRow);
Vue.component('ui-table-header', TableHeader);
Vue.component('ui-table-ceil', TableCeil);
