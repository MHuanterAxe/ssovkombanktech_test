import {
  currencyTransformer,
  dateTransformer,
} from '../../../utils/transformers';

export const tableColumnStyle = () => (column) => ({
  width: column.width,
});

export const formattedRowData = () => (type, value) => {
  switch (type) {
    case 'date': return dateTransformer(value);
    case 'currency': return currencyTransformer(value.toString());
    default: return value;
  }
};
