import Vue from 'vue';
import Vuex from 'vuex';
import api from '@/api';
import getMockedPayments from '@/mocks/getPayments';

Vue.use(Vuex);

const CACHE_KEY = 'CACHED_DATA';

export default new Vuex.Store({

  state: () => ({
    data: [],
    isLoading: false,
    isCached: false,
  }),

  mutations: {
    setState(state, value) {
      Object.entries(value).forEach(([key, data]) => {
        if (!Array.isArray(state[key]) && state[key] && typeof state[key] === 'object') {
          state[key] = {
            ...state[key],
            ...data,
          };
        } else {
          state[key] = data;
        }
      });
    },
  },

  actions: {
    async load({ commit, state }, params = {}) {
      if (localStorage.getItem(CACHE_KEY)) {
        const data = JSON.parse(localStorage.getItem(CACHE_KEY));
        commit('setState', { data, isCached: true });
        return;
      }

      commit('setState', { isLoading: true });

      try {
        const { data } = process.env.VUE_APP_API_URL ? await api.getPayments(params) : await getMockedPayments();

        if (Array.isArray(data)) {
          commit('setState', { data, isCached: true });
          localStorage.setItem(CACHE_KEY, JSON.stringify(data));
        }
      } catch (e) {
        // eslint-disable-next-line no-alert
        alert(e?.message);
      } finally {
        commit('setState', { isLoading: false });
      }
    },
    async clearCache({ commit, dispatch }) {
      commit('setState', { isCached: false });
      localStorage.removeItem(CACHE_KEY);
      await dispatch('load');
    },
  },
});
